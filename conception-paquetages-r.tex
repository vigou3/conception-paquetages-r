%%% Copyright (C) 2021-2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Conception de paquetages R»
%%% https://gitlab.com/vigou3/conception-paquetages-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{applekeys}                 % symboles Mac
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{dirtree}                   % arbre de fichiers
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX
  \usepackage{framed}
  \usepackage{fancyvrb}

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Conception de paquetages R}
  \author{Vincent Goulet}
  \renewcommand{\year}{2024}
  \renewcommand{\month}{08}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/conception-paquetages-r}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \colorlet{codebg}{LightYellow1}           % fond code R
  \colorlet{prompt}{Orchid4}                % invite de commande
  \colorlet{alert}{mLightBrown} % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias de couleur Metropolis
  \colorlet{code}{mLightGreen}  % alias de couleur Metropolis

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Rapports dynamiques avec Shiny},
    colorlinks = {true},
    linktocpage = {true},
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% =========================
  %%  Nouveaux environnements
  %% =========================

  %% Environnement pour le code informatique; hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \newenvironment{Scode}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Pour documenter les commandes des rubriques d'aide; dérivé de
  %% memoir.cls
  \def\bs{\code{\char`\\}}
  \newcommand{\cs}[1]{\code{\char`\\#1}}
  \newcommand{\meta}[1]{%
    \ensuremath\langle{\normalfont\itshape #1\/}\ensuremath\rangle}

  %% Renvoi vers GitLab sur la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \usepackage{relsize}
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\smaller\faExternalLink*}%
    \else
      \href{#1}{#2~\smaller\faExternalLink*}%
    \fi
  }

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\prompt}[1]{\textcolor{prompt}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs utilisées pour composer les couvertures
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}
  \frametitle{Objectifs}

  \begin{itemize}
  \item Concevoir un paquetage R simple regroupant des fonctions,
    leur documentation, des fichiers de données et des tests
    unitaires.
  \item Rédiger une rubrique d'aide consultable dans R.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sommaire}

  \begin{columns}
    \column{0.5\textwidth}
    \tableofcontents[sections=1-5,hideallsubsections]
    \column{0.5\textwidth}
    \tableofcontents[sections=6-10,hideallsubsections]
  \end{columns}
\end{frame}

%% mainmatter

\section{Introduction}

\begin{frame}
  \frametitle{Contexte}

  \begin{itemize}
  \item Vous avez développé des outils d'analyse de données avec R
  \item Vous souhaitez partager ces outils avec d'autres personnes:
    collègues de travail, organisation, univers\dots
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Écueils}

  Quelques questions que vous vous poserez:
  \begin{itemize}
  \item Comment facilement rendre disponible votre travail à
    plusieurs personnes?
  \item Comment s'assurer de l'intégrité de votre travail?
  \item Comment faire en sorte que la documentation demeure facilement
    accessible?
  \end{itemize}
  Avec R, la meilleure réponse est la création d'un
  \alert{paquetage}.
\end{frame}

\begin{frame}
  \frametitle{Paquetage R}

  \begin{itemize}
  \item Un paquetage est essentiellement un ensemble cohérent de
    \alert{fonctions}, de \alert{bases de données}, de \alert{tests
      unitaires} et de \alert{documentation}
  \item Un paquetage fait partie d'une \alert{bibliothèque}
    d'extensions au système R de base
  \item Plusieurs paquetages sont installés par défaut lors de
    l'installation de R
  \item Des paquetages additionnels sont installés par la suite pour
    répondre à de nouveaux besoins
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Paquetage vs code source brut}

  Pourquoi ne pas simplement distribuer du code source qui sera
  évalué avec \code{source}?

  \begin{itemize}
  \item Approche valable pour les petits projets \emph{ad hoc}
  \item Aucune gestion des éventuels conflits entre des
    outils du même nom
  \item Aucune gestion des dépendances
  \item Accès compliqué à la documentation (il faut
    consulter le fichier de script)
  \item Gestion manuelle du code source de tous les outils
  \item Diffusion difficile
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Documentation essentielle}

  Le seul ouvrage qui fait autorité quand il s'agit de paquetages R
  est le manuel officiel
  \link{https://cran.r-project.org/doc/manuals/R-exts.html}{\emph{Writing
      R Extensions}}.

  \begin{itemize}
  \item Distribué avec R, donc disponible sur votre poste de travail
    (dans RStudio, voir la sous-fenêtre Help --- \code{Ctrl+3}
    $\bullet$ \code{\ctlkey\,3})
  \item Exhaustif et à jour
  \item Ne vous avisez pas de poser une question dans les forums
    officiels sans avoir consulté la documentation
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \notebox{Brian D.~Ripley, un important contributeur de R et l'un des
    principaux auteurs de \emph{Writing R Extensions}, n'hésite pas,
    dans les forums, à rappeler sèchement aux personnes n'ayant pas lu
    la documentation qu'elles auraient dû le faire. Assez pour avoir
    fait naitre l'expression «\emph{To be Ripleyed}».}
\end{frame}


\section{Structure d'un paquetage}

\begin{frame}[fragile=singleslide]
  \frametitle{Généralités}

  À la base, un paquetage n'est qu'un \alert{répertoire} contenant des
  fichiers et des sous-répertoires, certains obligatoires, d'autres
  optionnels.

  \begin{itemize}
  \item Répertoire souvent nommé d'après le paquetage, mais pas
    obligatoirement
  \item Utile de placer ce répertoire dans un autre répertoire où le
    paquetage sera construit

    \begin{minipage}[t]{0.7\linewidth}
      \smaller
      \renewcommand*{\DTstylecomment}{\normalfont}
      \dirtree{%
        .1 \code{..}.
          .2 \code{projet/} \DTcomment{répertoire de travail}.
            .3 \code{pkg/} \DTcomment{répertoire du paquetage}.
              .4 \code{DESCRIPTION}.
              .4 \code{NAMESPACE}.
              .4 \code{R/}.
                .5 \code{foo.R}.
              .4 \code{man/}.
                .5 \code{foo.Rd}.
              .4 \code{tests/}.
                .5 \code{tests-foo.R}.
            .3 \code{pkg\_0.0-1.tar.gz} \DTcomment{paquetage construit}. }
    \end{minipage}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Éléments obligatoires}

  Les éléments suivants doivent \alert{obligatoirement} se trouver
  dans le répertoire du paquetage.

  \begin{itemize}
  \item fichier \code{DESCRIPTION}
  \item répertoire \code{R}
  \item répertoire \code{man}
  \item fichier \code{NAMESPACE}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Ce document est livré avec une ébauche (très avancée) de paquetage
  que vous compléterez au fil de la formation.
  \begin{enumerate}
  \item Déterminer l'emplacement du répertoire \code{pkg-exemple} dans
    votre système de fichiers, puis examiner son contenu.
  \item Démarrer la ligne de commande de votre système d'exploitation
    (Terminal ou \emph{Git Bash}) et naviguer jusqu'au répertoire
    \alert{parent} de \code{pkg-exemple}. (Autrement dit: faire du
    répertoire parent le répertoire courant.)
  \end{enumerate}
\end{frame}


\section{Fichier \code{DESCRIPTION}}

\begin{frame}
  \frametitle{Description générale du paquetage}

  Le fichier \code{DESCRIPTION} contient l'information de base à
  propos du paquetage: nom, version, auteurs, etc.

  \begin{itemize}
  \item Fichier de \alert{texte brut}
  \item Organisé en \alert{champs} de la forme \code{\meta{Nom}:
      \meta{valeur}}
  \item Texte en \alert{anglais}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple d'un fichier DESCRIPTION}

  \begin{Scode}
\begin{Verbatim}
Package: pkgname
Version: 0.1-1
Title: My First Collection of Functions
Author: Pam Beesly, Jim Halpert
Maintainer: Pam Beesly <pam@dunder-mifflin.net>
Description: Functions to compute some values useful in
  improbable research.
License: GPL (>=2)
Encoding: UTF-8
\end{Verbatim}
  \end{Scode}
\end{frame}

\begin{frame}
  \frametitle{Champs obligatoires}

  Les champs suivants doivent \alert{obligatoirement} se trouver dans
  le fichier \code{DESCRIPTION}:

  \begin{itemize}
  \item \code{Package}
  \item \code{Version}
  \item \code{Title}
  \item \code{Author}
  \item \code{Maintainer}
  \item \code{Description}
  \item \code{License}
  \item \code{Encoding} (si le fichier contient des caractères
    accentués)
  \end{itemize}
\end{frame}

\subsection{Champ \code{Package}}

\begin{frame}[fragile=singleslide]
  \frametitle{Nom du paquetage}

  Le champ \code{Package} détermine le \alert{nom} du paquetage.
  \begin{itemize}
  \item Uniquement des lettres (sans accents), des
    chiffres et le point
  \item Au moins deux caractères
  \item Débute par une lettre
  \item Ne se termine pas par un point
    \begin{Scode}
\begin{Verbatim}
Package: pkgname
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\subsection{Champ \code{Version}}

\begin{frame}[fragile=singleslide]
  \frametitle{Numéro de version}

  Le champ \code{Version} indique le \alert{numéro de version} du
  paquetage.
  \begin{itemize}
  \item Séquence d'au moins deux entiers
  \item Éléments séparés par «\code{.}» ou «\code{-}»
  \item Format standard: \code{x.y-z}
    \begin{Scode}
\begin{Verbatim}
Version: 0.1-1
\end{Verbatim}
    \end{Scode}
  \end{itemize}

  \tipbox{Le numéro de version n'est pas un nombre décimal. Ainsi, la
    version 0.9 vient avant la version 0.42, car $9 < 42$.}
\end{frame}

\subsection{Champ \code{Title}}

\begin{frame}[fragile=singleslide]
  \frametitle{Titre du paquetage}

  Le champ \code{Title} fournit une \alert{courte} description (une
  ligne) du paquetage.
  \begin{itemize}
  \item En quelque sorte le «slogan» du paquetage
  \item Règles anglaises pour les majuscules dans les titres
    (\link{https://apastyle.apa.org/style-grammar-guidelines/capitalization/title-case}{\emph{title
        case}})
  \item Ne pas répéter le nom du paquetage
  \item Ne pas terminer par un point
    \begin{Scode}
\begin{Verbatim}
Title: My First Collection of Functions
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\subsection{Champ \code{Author}}

\begin{frame}[fragile=singleslide]
  \frametitle{Auteurs du paquetage}

  Le champ \code{Author} fournit les noms des \alert{auteurs} du
  paquetage.
  \begin{itemize}
  \item Noms de toutes les personnes ayant été impliquées de manière
    significative dans le projet ou dont le code a été utilisé en
    vertu d'un contrat de licence libre
  \item Noms séparés par des virgules
    \begin{Scode}
\begin{Verbatim}
Author: Pam Beesly, Jim Halpert
\end{Verbatim}
    \end{Scode}
  \item Champ \code{Authors@R} offre plus de contrôle et permet de
    remplacer \code{Author}
  \end{itemize}
\end{frame}

\subsection{Champ \code{Maintainer}}

\begin{frame}[fragile=singleslide]
  \frametitle{Mainteneur du paquetage}

  Le champ \code{Maintainer} fournit le nom de la personne en charge
  de la \alert{maintenance} du paquetage.
  \begin{itemize}
  \item Personne-contact des administrateurs de CRAN
  \item Destinataire des rapports de bogues
  \item Nom suivi d'une adresse de courriel valide entre
    crochets obliques
    \begin{Scode}
\begin{Verbatim}
Maintainer: Pam Beesly <pam@dunder-mifflin.net>
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\subsection{Champ \code{Description}}

\begin{frame}[fragile=singleslide]
  \frametitle{Description du paquetage}

  Le champ \code{Description} fournit une \alert{description complète}
  du paquetage en quelques lignes (un paragraphe).
  \begin{itemize}
  \item Indique ce que le paquetage fait et dans quelles situations il
    est utile
  \item Permet d'expliquer le nom du paquetage
  \item Doit être intelligible pour tout le monde, surtout si le but
    est de publier le paquetage sur CRAN
  \item Ne pas débuter par le nom du paquetage, ni par «\emph{This
    package}\dots»
  \item Indenter les lignes \alert{après la première} d'au moins une
    espace
    \begin{Scode}
\begin{Verbatim}
Description: Functions to compute some values useful in
  improbable research.
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\subsection{Champ \code{License}}

\begin{frame}[fragile=singleslide]
  \frametitle{Contrat de licence}

  Le champ \code{License} indique le \alert{contrat de licence} du paquetage.
  \begin{itemize}
  \item Important d'indiquer le contrat de licence de votre travail
  \item Parfois un sujet complexe (notamment dans les organisations)
  \item Normalement le nom abrégé d'une licence libre «standard»
  \item Recommandation:
    \link{https://www.gnu.org/licenses/gpl-3.0.fr.html}{GNU Public
      License} (GPL) version 2 ou ultérieure
    \begin{Scode}
\begin{Verbatim}
License: GPL (>=2)
\end{Verbatim}
    \end{Scode}
  \end{itemize}

  \tipbox{\emph{Writing R Extensions} contient une
    \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Licensing}{section}
    entièrement dévolue au sujet du contrat de licence.}
\end{frame}

\subsection{Champ \code{Encoding}}

\begin{frame}[fragile=singleslide]
  \frametitle{Codage de caractères des fichiers}

  Le champ \code{Encoding} indique le type de codage de caractères du
  paquetage s'il contient des accents (caractères autres que
  \link{https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange}{ASCII}).
  \begin{itemize}
  \item Valide pour les fichiers \code{DESCRIPTION} et
    \code{NAMESPACE}, les fichiers de script R et les rubriques d'aide
  \item Même si le texte est en anglais, des accents peuvent se
    trouver dans les noms propres
  \item Utiliser le codage de caractères
    \link{https://fr.wikipedia.org/wiki/UTF-8}{UTF-8}
    \begin{Scode}
\begin{Verbatim}
Encoding: UTF-8
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Compléter l'ébauche de fichier \code{DESCRIPTION} se trouvant dans
  le répertoire \code{pkg-exemple}.

  \begin{enumerate}
  \item Ouvrir le fichier dans votre éditeur de texte.
  \item Inscrire un numéro de version significatif et compléter les
    champs \code{Title} et \code{Description}.
  \item Enregistrer le fichier complété.
  \end{enumerate}
\end{frame}


\section{Répertoire \code{R}}

\begin{frame}
  \frametitle{Code source R du paquetage}

  Le répertoire \code{R} contient le code source des \alert{fonctions}
  du paquetage.
  \begin{itemize}
  \item Uniquement des fichiers de script R
  \item Fichiers analysables avec \code{source}
  \item Nom des fichiers sans importance, mais extension \code{.R}
  \item Bonne pratique: un fichier par fonction, nommé d'après la
    fonction
  \end{itemize}

  \tipbox{Besoin des fonctions d'un autre paquetage? C'est dans le
    fichier \code{NAMESPACE} qu'il faut le dire. Les fichiers de
    script ne doivent pas contenir d'appels à \code{library}.}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Examiner le répertoire \code{R} de l'ébauche de paquetage dans
  \code{pkg-exemple}.

  \begin{enumerate}
  \item Remarquer que le répertoire ne contient que des fichiers de
    script.
  \item Remarquer que les fichiers de script ne contiennent que du
    code et des commentaires. La documentation des fonctions se
    trouve dans les rubriques d'aide correspondantes.
  \end{enumerate}
\end{frame}


\section{Répertoire \code{man}}

\begin{frame}
  \frametitle{Documentation du paquetage}

  Le répertoire \code{man} contient la documentation du paquetage.
  \begin{itemize}
  \item Uniquement des fichiers de documentation dans un format rigide
    propre à R
  \item Langage de balisage inspiré de \LaTeX
  \item Nom des fichiers sans importance, mais extension \code{.Rd}
  \item Tous les objets exportés par le paquetage doivent être
    documentés
  \end{itemize}
  Dans la suite nous nous attarderons uniquement à la documentation
  des \alert{fonctions}.

  \tipbox{Optez pour la simplicité et nommez le fichier de
    documentation du même nom que le fichier de script auquel il se
    rapporte.}
\end{frame}

\begin{frame}[plain]
  \notebox{Pourquoi \code{man} et non pas \code{doc} pour le nom du
    répertoire contenant la documentation? Il s'agit d'un vieux
    standard Unix, où l'on accède à la documentation du système depuis
    la ligne de commande avec l'utilitaire \code{man} --- la version
    abrégée de \emph{manual}.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple de fichier de documentation}

  Extraits de la rubrique d'aide de \code{?dnorm} du paquetage
  standard \pkg{stats}.
  \begin{Scode}
    \smaller[2]
\begin{Verbatim}
\name{Normal}
\alias{Normal}
\alias{dnorm}
[...]
\title{The Normal Distribution}
\description{
  Density, distribution function, quantile function and random generation for the normal
  distribution with mean equal to \code{mean} and standard deviation equal to \code{sd}.
}
\usage{
dnorm(x, mean = 0, sd = 1, log = FALSE)
pnorm(q, mean = 0, sd = 1, lower.tail = TRUE, log.p = FALSE)
[...]
}
\arguments{
  \item{x, q}{vector of quantiles.}
  \item{p}{vector of probabilities.}
  [...]
}
[...]
\end{Verbatim}
  \end{Scode}
\end{frame}

\begin{frame}
  \frametitle{Contenu d'un fichier .Rd}

  Principaux éléments à fournir dans la documentation:

  \begin{minipage}{0.4\linewidth}
    \smaller
    \begin{itemize}
    \item \cs{name}
    \item \cs{alias}
    \item \cs{title}
    \item \cs{description}
    \item \cs{usage}
    \end{itemize}
  \end{minipage}
  \textcolor{alert}{\vline width 1pt}\hfill
  \begin{minipage}{0.55\linewidth}
    entête obligatoire
  \end{minipage}

  \begin{minipage}{0.4\linewidth}
    \smaller
    \begin{itemize}
    \item \cs{arguments}
    \item \cs{details}
    \item \cs{value}
    \item \cs{references}
    \item \cs{seealso}
    \item \cs{examples}
    \end{itemize}
  \end{minipage}
  \textcolor{alert}{\vline width 1pt}\hfill
  \begin{minipage}{0.55\linewidth}
    corps optionnel (mais recommandé)
  \end{minipage}

  \begin{minipage}{0.4\linewidth}
    \smaller
    \begin{itemize}
    \item \cs{keyword}
    \end{itemize}
  \end{minipage}
  \textcolor{alert}{\vline width 1pt}\hfill
  \begin{minipage}{0.55\linewidth}
    pied de page (non traité ici)
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Identification de la rubrique d'aide}

  Une rubrique peut contenir la documentation de plusieurs fonctions.

  Les balises \cs{name} et \cs{alias} identifient le ou les
  «sujets» de la rubrique d'aide accessibles avec \code{?} ou
  \code{help}.
  \begin{itemize}
  \item \cs{name} est un identifiant \alert{unique} pour le paquetage,
    habituellement identique au nom du fichier \code{.Rd}
  \item Pas d'espace ou de symboles «\code{!}», «\code{\textbar}», «\code{@}» ou
    «\code{/}» dans le nom
  \item \cs{alias} identifie tous les sujets de la rubrique d'aide,
    \alert{y compris} celui donné dans \cs{name}
  \item Souvent plusieurs balises \cs{alias} dans un fichier
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple d'identification d'une rubrique d'aide}

  La documentation des fonctions \code{dnorm}, \code{pnorm},
  \code{qnorm}, \code{rnorm} du paquetage \pkg{stats} est regroupée
  dans le fichier \code{Normal.Rd}.

  Le fichier contient en entête:
  \begin{Scode}
\begin{Verbatim}
\name{Normal}
\alias{Normal}
\alias{dnorm}
\alias{pnorm}
\alias{qnorm}
\alias{rnorm}
\end{Verbatim}
  \end{Scode}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Titre de la rubrique d'aide}

  La balise \cs{title} définit le \alert{titre} de la rubrique d'aide.
  \begin{itemize}
  \item Affiché au tout début de la rubrique d'aide
  \item Règles anglaises pour les majuscules dans les titres
  \item Pas de point final
  \item Limiter à 65 caractères pour un maximum de compatibilité
\begin{Scode}
\begin{Verbatim}
\title{The Normal Distribution}
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Description des fonctions}

  La balise \cs{description} fournit une courte \alert{explication} de
  ce que font la ou les fonctions traitées dans la rubrique d'aide.
  \begin{itemize}
  \item Un paragraphe de quelques lignes tout au plus
  \item Complète la phrase «Cette fonction sert à calculer\dots»
    \begin{Scode}
\begin{Verbatim}
\description{
  Density, distribution function, quantile function and
  random generation for the normal distribution [...]
}
\end{Verbatim}
    \end{Scode}
  \end{itemize}

  \tipbox{Si \cs{description} devient trop long, c'est probablement
    parce que vous essayez de documenter trop de sujets en même temps.
    Séparez la documentation en plusieurs rubriques distinctes.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Signatures des fonctions}

  La balise \cs{usage} fournit les \alert{signatures} des fonctions.
  \begin{itemize}
  \item Une ligne par fonction
  \item Signature doit correspondre \alert{exactement} à la définition
    de la fonction dans le fichier de script
    \begin{Scode}
\begin{Verbatim}
\usage{
dnorm(x, mean = 0, sd = 1, log = FALSE)
pnorm(q, mean = 0, sd = 1, lower.tail = TRUE, log.p = FALSE)
qnorm(p, mean = 0, sd = 1, lower.tail = TRUE, log.p = FALSE)
rnorm(n, mean = 0, sd = 1)
}
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Arguments des fonctions}

  La balise \cs{arguments} fournit la description de \alert{tous les
    arguments} des fonctions mentionnées dans \cs{usage}.
  \begin{itemize}
  \item Arguments et descriptions définis avec des balises \cs{item}
    \begin{Scode}
\begin{Verbatim}[commandchars=+\[\]]
\item{+meta[argument]}{+meta[description]}
\end{Verbatim}
    \end{Scode}
  \item Noms des arguments doivent correspondre \alert{exactement} à
    la signature des fonctions
    \begin{Scode}
\begin{Verbatim}
\arguments{
  \item{x, q}{vector of quantiles.}
  \item{p}{vector of probabilities.}
  [...]
}
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Informations additionnelles}

  Deux balises optionnelles --- mais recommandées --- permettent de
  fournir de l'information additionnelle sur une fonction.
  \begin{itemize}
  \item \cs{details} pour les explications détaillées sur
    l'utilisation d'une fonction, ses arguments, son algorithme, etc.
  \item \cs{value} pour indiquer ce que retourne une fonction:
    valeur et type d'objet (vecteur, liste, etc.)
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemples d'utilisation}

  La balise \cs{examples} permet d'inclure dans la rubrique d'aide des
  \alert{exemples d'utilisation} des fonctions.
  \begin{itemize}
  \item Seulement des expressions, sans les résultats
  \item Commentaires permis
  \item Exemples valides seulements --- vérifié lors de la construction du
    paquetage
    \begin{Scode}
\begin{Verbatim}
\examples{
[...]
dnorm(0) == 1/sqrt(2*pi)
dnorm(1) == exp(-1/2)/sqrt(2*pi)
dnorm(1) == 1/sqrt(2*pi*exp(1))
[...]
}
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mise en forme du texte}

  Le format \code{.Rd} compte une panoplie de balises pour mettre en
  forme du texte:
  \begin{itemize}
  \item gras, italique
  \item code informatique
  \item équations mathématiques
  \item listes et tableaux
  \item hyperliens
  \item etc.
  \end{itemize}
  Consulter le
  \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Writing-R-documentation-files}{chapitre
    2} de \emph{Writing R Extensions} entièrement consacré à la
  préparation des rubriques d'aide.
\end{frame}

\begin{frame}
  \frametitle{Autres types de rubriques d'aide}

  Le système de documentation de R accepte des rubriques d'aide pour
  autre chose que des fonctions.
  \begin{itemize}
  \item \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Documenting-packages}{Sommaires
      de paquetage}
  \item \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Documenting-data-sets}{Jeux
      de données}
  \item \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Documenting-S4-classes-and-methods}{Méthodes}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Ouvrir les rubriques d'aide de l'ébauche de paquetage du répertoire
  \code{pkg-exemple}.
  \begin{enumerate}
  \item Étudier le contenu des fichiers, notamment les sections
    \cs{details} et \cs{seealso}.
  \item Examiner les équations mathématiques en ligne et hors
    paragraphe créées avec les commandes \cs{eqn} et \cs{deqn};
    consulter la
    \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Mathematics}{documentation}
    dans \emph{Writing R Extensions}.
  \item Compléter la description des arguments des fonctions.
  \item Consulter la prévisualisation des rubriques d'aide grâce au
    bouton Preview dans l'interface de RStudio.
  \end{enumerate}
\end{frame}


\section{Répertoire \code{tests}}

\begin{frame}
  \frametitle{Tests unitaires pour le code R}

  Le répertoire \code{tests} --- optionnel, mais fortement recommandé
  --- contient les \alert{tests unitaires} du paquetage.
  \begin{itemize}
  \item Idée: empêcher la construction du paquetage dès qu'un test
    échoue
  \item Fichiers de script R évalués lors de la construction
    du paquetage
  \item Tests évalués dans une nouvelle session R («vanille»): charger
    le paquetage avec \code{library}
  \item Tests pour des valeurs nominales des fonctions, les
    cas limites et les erreurs rencontrées dans le
    passé (pour éviter de réintroduire l'erreur dans le futur)
  \end{itemize}

  \tipbox{Si vos tests effectuent des simulations, pensez à
    réinitialiser l'amorce du générateur de nombres aléatoires avec
    \code{set.seed}.}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Étudier les fichiers de test unitaires de l'ébauche de paquetage du
  répertoire \code{pkg-exemple}.
\end{frame}


\section{Fichier \code{NAMESPACE}}

\begin{frame}[fragile=singleslide]
  \frametitle{Mise en situation (1/2)}

  Vous développez un paquetage R qui contient deux fonctions,
  \code{foo} et \code{bar}.
  \begin{itemize}
  \item \code{foo} fait appel à \code{bar}
    \begin{Scode}
\begin{Verbatim}
foo <- function(x, y) x * bar(y)
\end{Verbatim}
    \end{Scode}
  \item \code{foo} est destinée aux utilisateurs du paquetage
  \item \code{bar} est une fonction utilitaire interne
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mise en situation (2/2)}

  Quatre choses que vous voulez éviter:
  \begin{enumerate}
  \item Votre fonction \code{foo} écrase une fonction \code{foo} dans
    l'espace de travail ou dans un autre paquetage
  \item Une fonction \code{foo} dans l'espace de travail ou dans un
    autre paquetage écrase votre fonction \code{foo}
  \item Une autre fonction \code{bar} quelque part change le
    comportement de \code{foo}
  \item Les utilisateurs du paquetage peuvent appeler directement la
    fonction utilitaire \code{bar}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Éviter les conflits, isoler le code}

  Le fichier \code{NAMESPACE} définit l'\alert{espace de nommage} d'un
  paquetage.
  \begin{itemize}
  \item Fonctions \alert{exportées} par le paquetage et donc visibles par les
    utilisateurs
  \item Fonctions \alert{importées} d'autres paquetages
  \end{itemize}
  Toutes les fonctions non exportées sont automatiquement
  protégées par l'espace de nommage du paquetage.
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple de fichier \code{NAMESPACE}}

  \begin{Scode}
\begin{Verbatim}
### pkg: My First Collection of Functions
###
### AUTHORS: Pam Beesly, Jim Halpert
### LICENSE: GPL 2 or later.

## Imports
import(stats)

## Exports
export(foo, foobar)
\end{Verbatim}
  \end{Scode}

  \tipbox{Malgré les apparences, le contenu du fichier
    \code{NAMESPACE} n'est pas du code R.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exportation de fonctions}
  \begin{itemize}
  \item \code{export} indique les fonctions qu'un
    paquetage exporte
    \begin{Scode}
\begin{Verbatim}
export(foo, foobar)
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Importation de fonctions}
  \begin{itemize}
  \item \code{import} indique les paquetages externes qu'un paquetage
    importe (toutes les fonctions exportées par les paquetages
    externes sont importées)
    \begin{Scode}
\begin{Verbatim}
import(pkg)
\end{Verbatim}
    \end{Scode}
  \item \code{importFrom} restreint la liste des importations d'un
    paquetage externe aux fonctions mentionnées
    \begin{Scode}
\begin{Verbatim}
importFrom(stats, dnorm)
\end{Verbatim}
    \end{Scode}
  \end{itemize}

  \tipbox{Inutile d'importer le paquetage \pkg{base}, celui-ci est
    toujours chargé dans une session R.}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  Étudier le fichier \code{NAMESPACE} de l'ébauche de paquetage du
  répertoire \code{pkg-exemple}.
\end{frame}

\begin{frame}[plain]
  \tipbox{La fonction \code{package.skeleton} du paquetage standard
    \pkg{utils} permet de rapidement mettre en place toute la
    structure d'un paquetage à partir d'une liste d'objets à exporter.
    Pratique!}
\end{frame}


\section[Construction et validation \\ d'un paquetage]{Construction et validation d'un paquetage}

\begin{frame}[fragile=singleslide]
  \frametitle{Construction et validation de paquetage}

  Une fois tous éléments en place, il faut encore \alert{construire}
  et \alert{valider} un paquetage avant de pouvoir le distribuer et
  l'installer.
  \begin{itemize}
  \item Opérations effectuées depuis la ligne de commande du système
    d'exploitation
  \item Commande \code{R CMD build} --- avec en argument le nom du
    \alert{répertoire} contenant le code source --- construit le paquetage
    sous forme d'un fichier compressé \code{.tar.gz}
    \begin{Scode}
\begin{Verbatim}[commandchars=\\\{\}]
\prompt{$} R CMD build \meta{dir}
\end{Verbatim}
    \end{Scode}
  \item Commande \code{R CMD check} --- avec en argument le nom du
    \alert{fichier} \code{.tar.gz} --- valide la structure et le contenu
    du paquetage
    \begin{Scode}
\begin{Verbatim}[commandchars=\\\{\}]
\prompt{$} R CMD check \meta{pkg}.tar.gz
\end{Verbatim}
    \end{Scode}
  \item Validation réussie lorsqu'il n'y a ni \alert{erreur}, ni
    \alert{avertissement}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{FAQ 42}

  Qu'est-ce qui cause cette erreur lors de la validation de mon
  paquetage?
  \begin{Scode}
    \smaller
\begin{Verbatim}[commandchars=\\\{\}]
\prompt{$} R CMD check pkg_1.0-3.tar.gz
* using log directory ‘C:/Users/dwight/Documents/Université/Analyse de données/
conception-paquetages-r/pkg.Rcheck’
[...]
* checking whether package ‘pkg’ can be installed ... ERROR
Installation failed.
See ‘C:/Users/dwight/Documents/Université/Analyse de données/
conception-paquetages-r/pkg.Rcheck/00install.out’ for details
\end{Verbatim}
  \end{Scode}

  \warningbox{Sous Windows, la commande \code{R CMD check} échoue si
    le chemin d'accès absolu (à partir de la racine du disque) vers le
    fichier \code{\meta{pkg}.tar.gz} contient des accents. Veillez à
    placer le fichier dans un répertoire sans aucune lettre accentuée
    dans son chemin d'accès avant de valider le paquetage.}
\end{frame}

\begin{frame}[plain]
  \notebox{\textbf{La minute \emph{geek}}

    Un fichier \code{.tar.gz} est un fichier d'archive compressée
    standard de Unix. C'est l'ancêtre du format \code{.zip}. Dans la
    plus pure tradition Unix, l'archive est créée par deux utilitaires
    différents.

    Tout d'abord, il y a \code{tar} (pour \emph{tape archive})
    qui regroupe plusieurs fichiers en un seul en vue de les archiver
    (à l'origine sur un ruban magnétique, d'où le nom).

    Ensuite, pour économiser de l'espace, le fichier est compressé
    avec l'utilitaire \code{gzip}.

    Résultat: un fichier \code{.tar.gz} ou \code{.tgz}.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Construire et valider le paquetage \pkg{pkg} du répertoire
  \code{pkg-exemple} à l'aide des commandes suivantes (inscrire le
  numéro de version figurant dans le fichier \code{DESCRIPTION}):
    \begin{Scode}
\begin{Verbatim}[commandchars=\\\{\}]
\prompt{$} R CMD build pkg-exemple
\prompt{$} R CMD check pkg_\meta{version}.tar.gz
\end{Verbatim}
    \end{Scode}
\end{frame}


\section{Installation d'un paquetage}

\begin{frame}[fragile=singleslide]
  \frametitle{Installation du paquetage}

  Un paquetage construit, c'est bien. Un paquetage installé dans une
  bibliothèque, c'est mieux.
  \begin{itemize}
  \item (Option la plus simple et rapide) Depuis la ligne de commande
    du système d'exploitation avec \code{R CMD INSTALL}
    \begin{Scode}
\begin{Verbatim}[commandchars=\\\{\}]
\prompt{$} R CMD INSTALL \meta{pkg}.tar.gz
\end{Verbatim}
    \end{Scode}
  \item Depuis une session R avec \code{install.packages} comme
    d'habitude, mais sans aller chercher le paquetage sur CRAN
    \begin{Scode}
\begin{Verbatim}[commandchars=\\\{\}]
install.packages("\meta{pkg}.tar.gz", \alert{repos = NULL})
\end{Verbatim}
    \end{Scode}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  \begin{enumerate}
  \item Installer le paquetage \pkg{pkg} dans votre bibliothèque
    personnelle.
  \item Vérifier la disponibilité des fonctions dans R.
  \item Consulter les rubriques d'aide du paquetage.
  \end{enumerate}
\end{frame}


\section{Aller plus loin}

\begin{frame}
  \frametitle{Morceaux choisis pour développeurs avancés}

  \begin{itemize}
  \item Un fichier \code{NEWS.Rd} placé dans le répertoire \code{inst}
    --- et lu par la fonction \code{news} --- permet de conserver les
    notes de version du paquetage.
  \item Les
    \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Writing-package-vignettes}{vignettes}
    sont des documents rédigés en programmation lettrée avec Sweave
    (par défaut) ou knitr qui proposent de la documentation longue en
    format PDF ou HTML.
  \item Un paquetage peut contenir des
    \link{https://cran.r-project.org/doc/manuals/R-exts.html\#Internationalization}{traductions}
    de ses divers messages (internationalisation, ou «i18n»).
  \item Pour les calculs numériques intensifs nécessitant plus de
    vitesse, il est possible d'appeler du
    \link{https://cran.r-project.org/doc/manuals/R-exts.html\#System-and-foreign-language-interfaces}{code
      C ou Fortran depuis R} (\emph{assemblage requis} --- exemple
    livré avec le paquetage \pkg{expint}).
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \centering
  \includegraphics[height=0.9\textheight,keepaspectratio]{images/dependency} \\
  \footnotesize Tiré de \href{https://xkcd.com/2347/}{XKCD.com}
\end{frame}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
