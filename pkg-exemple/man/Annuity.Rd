\name{Annuity}
\alias{Annuity}
\alias{pres.value}
\title{Present Value of Level Payment Annuities}
\description{
  Present value of immediate or due annuities with level payments, or
  perpetuities.
}
\usage{
pres.value(n, i, due = FALSE)
}
\arguments{
  \item{n}{<insert description of argument here>}
  \item{i}{<insert description of argument here>}
  \item{due}{<insert description of argument here>}
}
\details{
  The present value of \eqn{n}-payment, end-of-period (or
  \emph{immediate}), annuity with level of \eqn{1} is
  \deqn{v + v^2 + v^3 + \dots + v^n = \frac{1 - v^n}{i},}{%
    v + v^2 + v^3 + ... + v^n = (1 - v^n)/i,}
  where \eqn{v = 1/(1 + i)}.

  For beginning-of-period (or \emph{due}) annuities, the \eqn{i} in the
  denominator above is replaced by \eqn{d = 1/(1 + i)}.
}
\value{
  Vector of present values.
}
\examples{
## 10-period annuity immediate with an interest rate of 5%
pres.value(10, 0.05)

## Same, but annuity due
pres.value(10, 0.05, due = TRUE)

## Perpetuity due, still at 5% interest rate
pres.value(Inf, 0.05, due = TRUE)

## With no interest, present value is equal
## to the number of payments.
pres.value(10, 0)

## Function is vectorial
pres.value(c(10, 5, Inf), i = c(0.05, 0, 0.03))
}

