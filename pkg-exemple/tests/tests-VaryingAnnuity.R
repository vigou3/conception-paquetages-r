### pkg: <package title>
###
### Tests for incr.pres.value and dec.pres.value functions.
###
### AUTHORS: Jean-Christophe Langlois, Vincent Goulet <vincent.goulet@act.ulaval.ca>
### LICENSE: GPL 2 or later.

library(pkg)

## Local copy of tools:assertError.
assertError <- tools::assertError

## Checks for invalid arguments.
assertError(incr.pres.value(-5, 0.05))
assertError(incr.pres.value(2, -0.05))
assertError((incr.pres.value(c(-2, 3), c(0.05, 0.06))))
assertError((incr.pres.value(c(2, 3), c(0.05, -0.06))))

assertError(decr.pres.value(-5, 0.05))
assertError(decr.pres.value(2, -0.05))
assertError(decr.pres.value(Inf, 0.05))
assertError((decr.pres.value(c(-2, 3), c(0.05, 0.06))))
assertError((decr.pres.value(c(2, 3), c(0.05, -0.06))))

## Limit cases: single period annuities (immediate and due
## annuities); perpetuity; zero interest rate.
stopifnot(exprs = {
    all.equal(1/1.05, incr.pres.value(1, 0.05))
    all.equal(1, incr.pres.value(1, 0.05, TRUE))
    all.equal(1.05/0.05^2, incr.pres.value(Inf, 0.05))
    all.equal(55, incr.pres.value(10, 0))

    all.equal(1/1.05, decr.pres.value(1, 0.05))
    all.equal(1, decr.pres.value(1, 0.05, TRUE))
    all.equal(55, decr.pres.value(10, 0))
})

## Positive interest rates, many periods to check that functions are
## vectorial.
stopifnot(exprs = {
    all.equal(c(1/1.05, 1/1.06 + 2/1.06^2),
              incr.pres.value(c(1, 2), c(0.05, 0.06)))
    all.equal(c(1, 1 + 2/1.06),
              incr.pres.value(c(1, 2), c(0.05, 0.06), TRUE))

    all.equal(c(1/1.05, 2/1.06 + 1/1.06^2),
              decr.pres.value(c(1, 2), c(0.05, 0.06)))
    all.equal(c(1, 2 + 1/1.06),
              decr.pres.value(c(1, 2), c(0.05, 0.06), TRUE))
})
